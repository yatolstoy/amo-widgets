require('dotenv').config();

const {APP_PORT} = process.env;
const publicPath = `https://localhost:${APP_PORT}`;

module.exports = {
	publicPath,
	configureWebpack: {
		output: {
			library: 'amocrm-widget',
			libraryTarget: 'umd'
		}
	},
	chainWebpack: (config) => {
		config.plugins.delete('html');
		config.plugins.delete('preload');
		config.plugins.delete('prefetch');
		config.optimization.delete('splitChunks');
	},
	devServer: {
		https: true,
		port: APP_PORT,
		allowedHosts: "all",
		headers: { 'Access-Control-Allow-Origin': '*' }
	}
}