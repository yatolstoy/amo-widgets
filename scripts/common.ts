import { Config } from "amo-widget-builder";
import * as path from "path";

export const config: Config = {
  name: {
    ru: "Раскрашивание сделок",
  },
  description: {
    ru: "Раскрашивает сделки от 100 000 руб в зелёный цвет - description",
  },
  shortDescription: {
    ru: "Раскрашивает сделки от 100 000 руб в зелёный цвет - shortDescription",
  },
  tourDescription: {
    ru: "Раскрашивает сделки от 100 000 руб в зелёный цвет - tourDescription",
  },
  advancedSettingsTitle: {
    ru: "Раскрашивание сделок",
  },
  version: "1.0.0",
  fakeConfig: {
    required: false,
  },
  locales: ["ru"],
  outDir: path.resolve(__dirname, ".."),
  bundleDir: path.resolve(__dirname, "../dist"),
};
