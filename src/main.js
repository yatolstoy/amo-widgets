 import { patchRouter } from './router';
 import { paintOnInit } from './features/colorLeadCards'

const init = () => {
	paintOnInit();
	patchRouter();
	return true;
};

const render = () => {
	return true;
}

export default function (widget) {
	widget.callbacks = {
		settings: function ($box) {
			if (widget.params.status === 'not_configured') {
				widget.background_install();
			}
		},
		init,
		bind_actions: () => true,
		render,
		dpSettings: function () {},
		advancedSettings: function () {},
		destroy: () => true,
		contacts: {
			selected: function () {}
		},
		onSalesbotDesignerSave: function (handler_code, params) {},
		leads: {
			selected: function () {}
		},
		todo: {
			selected: function () {}
		},
		onSave: function () {
			return true;
		},
		onAddAsSource: function (pipeline_id) {}
	}
	return widget;
}