import { addPipelineObserverRouter } from './pages/pipeline'

export function patchRouter() {
  const handler = {
    apply: function(target, thisArg, argumentsList) {
      const pathname = location.pathname;
      if (pathname.match(/^\/leads\/pipeline\//)) {
        addPipelineObserverRouter();
      }
      return target.apply(thisArg, argumentsList);
    }
  };
  APP.router.routeLoadedThenTrigger = new Proxy(APP.router.routeLoadedThenTrigger, handler);
}
