import { paintOnInit } from '../features/colorLeadCards'

let pipelineObserver
export function addPipelineObserverRouter() {
  // Удаляем обзервер, если он есть
  if (pipelineObserver) pipelineObserver.disconnect()

  const target = document.querySelector('body');

  // Устанавливаем функцию наблюдателя
  pipelineObserver = new MutationObserver((mutationsList, observer) => {
    mutationsList.forEach(mutation => {
      mutation.addedNodes.forEach(node => {
        // Если искомая страница вставлена - выключаем наблюдатель
        if (node.id === 'page_holder') {
          paintOnInit()
          observer.disconnect()
        }
      })
    })
  })

  // Включаем наблюдатель
  const config = { childList: true, subtree: true };
  pipelineObserver.observe(target, config);
}
