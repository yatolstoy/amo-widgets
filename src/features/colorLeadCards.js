import {getLeads, isLeadsPipeline, findLeadById} from '../common/amocrm'

const paintRule = lead => lead?.budget > 100000;

const paintLead = (lead) => {
	const card = document.getElementById(`pipeline_item_${lead.id}`);
	card.style["background-color"] = '#c7ffc4';
}

const unpaintLead = (lead) => {
	const card = document.getElementById(`pipeline_item_${lead.id}`);
	card.style["background-color"] = null;
}

export const paintController = (lead) => {
	if(paintRule(lead)) {
		paintLead(lead);
	} else {
		unpaintLead(lead);
	}
}

export const paintOnInit = () => {
	if(!isLeadsPipeline()) return;
	const leads = getLeads();
	leads.filter(paintRule).forEach(paintLead);
	pipelinePaintListener();
}

let pipelinePaintObserver
export const pipelinePaintListener = () => {
	if(pipelinePaintObserver) pipelinePaintObserver.disconnect();

	const targetNode = document.getElementById('pipeline_holder');

	const config = { childList: true, subtree: true,  };

	const callback = (mutationList, observer) => {
		for (const mutation of mutationList) {
			mutation.addedNodes.forEach(paintNodeFilter);
		}
	};

	pipelinePaintObserver = new MutationObserver(callback);

	pipelinePaintObserver.observe(targetNode, config);
}

const paintNodeFilter = addedNode => {
	const classesAddedNodes = addedNode?.classList;
	if (classesAddedNodes?.contains('pipeline_leads__item') 
			|| classesAddedNodes?.contains('pipeline_leads__info')) {
		const card = addedNode.closest('.pipeline_leads__item');
		const lead = findLeadById(card.dataset.id);
		paintController(lead);
	}
}