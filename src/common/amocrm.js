export const isLeadsPipeline = () => APP.data.current_entity === 'leads-pipeline';

export const getLeads = () => {
	return APP.data.current_view?._items?.models.map(el => el.attributes);
}

export const findLeadById = (id) => {
	const leads = getLeads();
	return leads.find(el => +el.id === +id);
}